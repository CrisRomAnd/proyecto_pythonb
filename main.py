import getpass
import aerolinea.vista_aerolinea as view
import aerolinea.Persona as p
import aerolinea.Vuelos as v


if __name__ == '__main__':
    pers = p.Persona()
    vuel = v.Vuelos()
    vuel.aniadir_vuelo('Toronto', '06:00', '13:00', 200,
                       200, 1000, 20, 20, 10, '2019/07/15')
    id = None
    pers.aniadir_persona('admin', 'Cristo', 'De Neza', '1234', 1)
    pers.aniadir_persona('user', 'Cristian', 'Romero', '1234', 2)
    print(view.session)
    print('\t\t Ingrese "h" para que aparescan las opciones')
    while True:
        pt = input(view.puntero)
        if pt == '1':
            print(view.inicio_admin)
            while True:
                pt = input(view.puntero)
                if pt == '1':
                    user = input('\tUsuario:' + view.puntero)
                    pwd = getpass.getpass('\tContraseña:' + view.puntero)
                    id = pers.get_id(user)
                    val_1 = pers.val_admin(id)
                    val_2 = pers.val_session(id, pwd)
                    if val_1 and val_2:
                        print('\tIngresando...')
                        print(view.man_admin)
                        while True:
                            pt = input(view.puntero)
                            if pt == '1':
                                num_max_vuelo = int(input('\tDame el número máximo de vuelos'
                                                          + view.puntero))
                                vuel.set_max_vuelos(num_max_vuelo)
                            elif pt == '2':  # ingresar vuelos
                                destino = input('\t¿Destino?' + view.puntero)
                                h_sal = input(
                                    '\t¿Hora de Salida?' + view.puntero)
                                h_ent = input(
                                    '\t¿Hora de Llegada?' + view.puntero)
                                cst_turista = float(
                                    input('\t¿Costo Clase Turista?' + view.puntero))
                                cst_negocio = float(
                                    input('\t¿Costo Clase Negocios?' + view.puntero))
                                cst_pc = float(
                                    input('\t¿Costo Primera Clase?' + view.puntero))
                                lug_turista = int(
                                    input('\t¿Lugares de Clase Turista?' + view.puntero))
                                lug_negocio = int(
                                    input('\t¿Lugares de Clase Negocio?' + view.puntero))
                                lug_pc = int(
                                    input('\t¿Lugares de Clase Primera Clase?' + view.puntero))
                                f_salida = input(
                                    '\t¿Fecha de Salida?' + view.puntero)
                                respuesta = vuel.aniadir_vuelo(destino, h_sal, h_ent, cst_turista,
                                                               cst_negocio, cst_pc, lug_turista,
                                                               lug_negocio, lug_pc, f_salida)
                                print('¡\n\tAñadido!') if respuesta \
                                    else print('\n\tHubo un Error...')
                            elif pt == '3':  # Listar Vuelos
                                print(vuel.listar_vuelos())
                            elif pt == '4':  # Cancelar Vuelos
                                print(vuel.listar_vuelos() +
                                      '\n\t¿Qué vuelo quiere cancelar?')
                                vuel.cancelar_vuelo(int(input(view.puntero)))
                                print('\t¡Cancelado!')
                            elif pt == '5':
                                print(vuel.listar_vuelos() +
                                      '\n\t¿Qué vuelo quiere modificar?')
                                ide = int(input(view.puntero))
                                print(vuel.listar_datos())
                                op = input(view.puntero)
                                if op == '0':
                                    print('\n\tDestino Actual: ' +
                                          vuel.get_destino(ide) + '\n\tNuevo')
                                    vuel.actualizar_destino(
                                        ide, input(view.puntero))
                                    print('\n\t¡Actualizado!')

                                elif op == '1':
                                    print('\n\tHora de Salida Actual: ' +
                                          vuel.get_hora_salida(ide) + '\n\tNuevo')
                                    vuel.actualizar_hora_salida(
                                        ide, input(view.puntero))
                                    print('\n\t¡Actualizado!')

                                elif op == '2':
                                    print('\n\tHora de Llegada Actual: ' +
                                          vuel.get_hora_llegada(ide) + '\n\tNuevo')
                                    vuel.actualizar_hora_llegada(
                                        ide, input(view.puntero))
                                    print('\n\t¡Actualizado!')

                                elif op == '3':
                                    print(
                                        '\n\tCosto Turista: ' + vuel.get_costo_clase_turista(ide) + '\n\tNuevo')
                                    vuel.actualizar_costo_turista(
                                        ide, float(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '4':
                                    print(
                                        '\n\tCosto Negocios: ' + vuel.get_costo_clase_negocios(ide) + '\n\tNuevo')
                                    vuel.actualizar_costo_negocios(
                                        ide, float(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '5':
                                    print('\n\tCosto Primera Clase: ' + vuel.get_costo_primera_clase(ide) +
                                          '\n\tNuevo')
                                    vuel.actualizar_costo_primera_clase(
                                        ide, float(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '6':
                                    print('\n\tLugares Turista: ' +
                                          vuel.get_lugares_turista(ide) + '\n\tNuevo')
                                    vuel.actualizar_lugares_turista(
                                        ide, int(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '7':
                                    print(
                                        '\n\tLugares Negocios: ' + vuel.get_lugares_negocios(ide) + '\n\tNuevo')
                                    vuel.actualizar_lugares_negocios(
                                        ide, int(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '8':
                                    print('\n\tLugares Primera Clase: ' + vuel.get_lugares_primera_clase(ide) +
                                          '\n\tNuevo')
                                    vuel.actualizar_lugares_primera_clase(
                                        ide, int(input(view.puntero)))
                                    print('\n\t¡Actualizado!')

                                elif op == '9':
                                    print('\n\tFecha Salida: ' +
                                          vuel.get_fecha_salidas(ide) + '\n\tNuevo')
                                    vuel.actualizar_fecha_salida(
                                        ide, input(view.puntero))
                                    print('\n\t¡Actualizado!')

                                elif op == '10':
                                    print('\n\tEstado: ' +
                                          vuel.get_estado(ide) + '\n\t Nuevo')
                                    vuel.actualizar_estado(
                                        ide, input(view.puntero))
                                    print('\n\t¡Actualizado!')

                            elif pt == '6':
                                print(view.est_pago)
                                while True:
                                    op = input(view.puntero)
                                    if op == '1':
                                        print('\n\tActualmente hay: ${0} en efectivo'.
                                              format(vuel.get_caja()))

                                    elif op == '2':
                                        print('\n\tActualmente hay: ${0} de Saldo en tarjeta'.
                                              format(vuel.get_caja_tarjeta()))

                                    elif op == '3':
                                        print('\n\tEn Total hay ${0}'.
                                              format(vuel.get_caja() + vuel.get_caja_tarjeta()))

                                    elif op == '4':
                                        print('\n\tSe hicieron {0} pagos en efectivo'.
                                              format(vuel.get_pagos_efectivo()))

                                    elif op == '5':
                                        print('\n\tSe hicieron {0} pagos con Tarjeta'.
                                              format(vuel.get_pagos_tarjeta()))

                                    elif op == '6':
                                        print(view.man_admin)
                                        break

                                    elif op == 'h':
                                        print(view.est_pago)

                                    else:
                                        continue

                            elif pt == '7':
                                print(view.est_clase)
                                while True:
                                    op = input(view.puntero)
                                    if op == '1':
                                        print('\n\tBoletos Vendidos en clase turista: {0}'.
                                              format(vuel.get_vendidos_turista()))

                                    elif op == '2':
                                        print('\n\tBoletos Vendidos en clase negocios: {0}'.
                                              format(vuel.get_vendidos_negocios()))

                                    elif op == '3':
                                        print('\n\tBoletos Vendidos en Primera Clase: {0}'.
                                              format(vuel.get_vendidos_primera_clase()))

                                    elif op == '4':
                                        dic_grande = {
                                            'Turista': vuel.get_vendidos_turista(),
                                            'Negocios': vuel.get_vendidos_negocios(),
                                            'Primera Clase': vuel.get_vendidos_primera_clase()
                                        }

                                        grande = max(list(dic_grande.values()))
                                        lista = []
                                        for i in range(len(list(dic_grande.values()))):
                                            if dic_grande[list(dic_grande.keys())[i]] == grande:
                                                lista.append(
                                                    list(dic_grande.keys())[i])
                                        print(
                                            '\n\tLa clase(s) más vendida(s) es(son):', lista)

                                    elif op == '5':
                                        print(view.man_admin)
                                        break

                                    elif op == 'h':
                                        print(view.est_clase)

                                    else:
                                        continue

                            elif pt == '8':
                                print(pers.info_usuario(id))
                            elif pt == '9':
                                print(view.inicio_admin)
                                break
                            elif pt == 'h':
                                print(view.man_admin)
                            else:
                                print(view.puntero)
                    else:
                        print('\tUsuario no valido!!!')
                        user = ''
                        print(view.inicio_admin)
                elif pt == '2':
                    print(view.session)
                    break
                elif pt == 'h':
                    print(view.inicio_admin)
                else:
                    continue

        elif pt == '2':  # -- Cliente
            print(view.inicio_cli)
            while True:
                pt = input(view.puntero)
                if pt == '1':
                    user = input('\tUsuario:' + view.puntero)
                    pwd = getpass.getpass('\tContrsenia:' + view.puntero)
                    id_user = pers.get_id(user)
                    if pers.val_session(id_user, pwd):
                        print(view.man_cli)
                        while True:
                            op = input(view.puntero)
                            if op == '1':
                                print(vuel.listar_vuelos_disponibles())

                            elif op == '2':
                                print(vuel.listar_vuelos_disponibles())
                                id_v = input(
                                    '\tSeleccione el vuelo deseado' + view.puntero)
                                id_cl = input(
                                    '\tDeme la clase deseada\n\t1. Turista\n\t2. Negocios\n\t3. Primera Clase' + view.puntero)

                                id_pago = input(
                                    '\tpagar con\n\t1. Efectivo\n\t2. Tarjeta' + view.puntero)
                                if vuel.comprar_vuelo(id_v, id_user, id_cl, id_pago):
                                    print('\tEl Vuelo {} de la Clase {} ha sido ingresado con exito'.format(
                                        id_v, id_cl))
                                else:
                                    print('\tEl Vuelo {} de la Clase {} ha sido NO se efectuo, F'.format(
                                        id_v, vuel.get_clase_by_id(id_cl)))

                            elif op == '3':
                                print(pers.info_usuario(id_user))
                                print('\tVuelos por identificador: ' + vuel.vuelos_persona(id_user))
                            elif op == '4':
                                print(view.inicio_cli)
                                break
                            elif op == 'h':
                                print(view.man_cli)
                    else:
                        print('\tUsusario invalido...')
                        user = ''
                        id = None
                        print(view.inicio_cli)

                elif pt == '2':
                    nombre = input('\tNombre' + view.puntero)
                    apellido = input('\tApellido' + view.puntero)
                    usuario = input('\tUsuario' + view.puntero)
                    contra = getpass.getpass('\tContraseña' + view.puntero)

                    pers.aniadir_persona(usuario, nombre, apellido, contra, 2)
                    print('\tRegistrado:', pers.ultimo_registrado())
                    print(view.inicio_cli)
                elif pt == '3':
                    print(view.session)
                    break
                elif pt == 'h':
                    print(view.inicio_cli)
                else:
                    continue

        elif pt == 'q':
            print('Bye...')
            break
        elif pt == 'h':
            print(view.session)
        else:
            continue
