# Proyecto_PythonB

Proyecto Python sobre vuelos y ventas

## SISTEMA DE VENTAS

**Objetivo**: Realizar un sistema capaz de manejar la venta de boletos
y adminstración de vuelos e ingresos para el personal del aeropuerto.

### Esquema del Proyecto

El proyecto deberá contar con el siguiente menú inicial

![Menu Inicial](img/minicial.png)

#### Administrador

Al seleccionar la opción uno del sistema de ventas se despliega el siguiente menú:

![Menu Admin](img/madmin.png)

Al seleccionar la opción uno de administrador se despliega la siguiente operación la cual pide
nombre de administrador y contraseña. 

![Menu Admin](img/madmin1.png)

Y solamente deja ingresar si los datos ingresados son correctos; si son erróneos le muestra un
aviso al usuario:

![Menu Admin](img/madmin2.png)

Este administrador lo tienen que crear desde el código, en ningún lugar del programa se registran
los administradores. Al ingresar como administrador le da la opción de manejar todo el sistema
de ventas del aeropuerto con el siguiente menú:

![Menu Admin](img/madmin3.png)

A continuación se detalla qué hace cada opción del menú:

**Opción 1**. Se ingresa el número máximo de vuelos que el aeropuerto tendrá disponibles para dar
de alta en el sistema.

**Opción 2**. Se capturan los datos para cada vuelo, con el siguiente formulario

![Menu Admin](img/madmin4.png)


**Opción 3**. Realiza un listado de vuelos que se han dado de alta con todos sus datos, de la siguiente
forma:

![Menu Admin](img/madmin5.png)

**Opción 4**. Elimina un vuelo que se haya dado de alta. Se tiene que dar la opción de elegir que
vuelo es el que se quiere eliminar.

**Opción 5**. Actualiza cualquier dato de los vuelos disponibles en el sistema con el siguiente menú

![Menu Admin](img/madmin6.png)

Dar la opción de elegir cual vuelo es que se quiere editar después de seleccionar alguna opción
del menú.

![Menu Admin](img/madmin7.png)

**Opción 6**. Muestra las estadísticas de pago, se sugiere utilizar el siguiente menú:

![Menu Admin](img/madmin8.png)

Todas las estadísticas mostradas en este menú se generan por las transacciones que realizar el
usuario al comprar un vuelo, ya que se le da la opción de pagar con tarjeta o con efectivo.

**Opción 7**. Se muestran todas las estadísticas relacionadas con los boletos vendidos de acuerdo al
siguiente menú:

![Menu Admin](img/madmin10.png)

Estas estadísticas se generan para todos los vuelos dados de alta.

**Opción 8**. Solamente se muestran los datos del administrador, como nombre apellido, sueldo, etc.

#### Cliente

Al seleccionar la opción dos del sistema de ventas se despliega el siguiente menú: 

![Menu Cli](img/mcliente.png)

La opción de registrarse permite dar de alta a un nuevo usuario pidiendo los siguientes datos:

![Menu Cli](img/mcliente1.png)


Y posteriormente vamos a poder ingresar (con la opción **uno**) a nuestro sistema de compras con
nuestro nuevo usuario y contraseña, donde vemos el siguiente menú:

![Menu Cli](img/mcliente2.png)

A continuación se detalla en qué consiste cada opción del menú:

**Opción 1**. Realiza un listado de los vuelos que el administrador ha dado de alta previamente.

**Opción 2**. Realiza un listado de los vuelos que el administrador ha dado de alta previamente y
muestra la opción de poder comprar algún vuelo de la siguiente forma:

![Menu Cli](img/mcliente3.png)

En la opción *Ingrese el número del vuelo* tienes que ingresar el vuelo a comprar y posterior a esto
se muestran los siguientes menús, donde tendrás que elegir el tipo de Clase y la Forma de pago:

![Menu Cli](img/mcliente4.png)

Al realizar esta compra se tienen que ver reflejadas las modificaciones en el lado del
**administrador**.

**Opción 3**. Realiza una impresión de los datos del usuario que ha iniciado sesión.

##### Notas

* El proyecto requiere un gran manejo de estructuras de control, métodos para realizar todas
las operaciones de dinero y manejo de Listas.

* Para el tema de la contraseña que los usuarios ingresan: Ésta puede verse al momento que
se ingresa, sin embargo, se considerará **2 puntos extra sobre calificación del proyecto**
si logran que no se vea nada cuando se teclea. Claramente esto cuenta como
“Investigación propia”.




