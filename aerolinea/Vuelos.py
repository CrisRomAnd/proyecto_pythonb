class Vuelos:
    """Abstrae los vuelos

    Se simunla los boletos, lugares de cada vuelo
    """

    identificador = 1
    __vuelo = dict()
    __max_vuelos = 10
    __caja = float(0)
    __caja_tarjeta = float(0)
    __pagos_tarjeta, __pagos_efectivo = 0, 0
    __vendidos_primera_clase, __vendidos_turista = 0, 0
    __vendidos_negocios = 0
    __vuelo_persona = {}
    """id = [persona]"""

    plantilla_datos = ['Destino',
                       'Hora de salida',
                       'Hora de llegada',
                       'Costo clase Turista',
                       'Costo clase Negocios',
                       'Costo Primera clase',
                       'Lugares Turista',
                       'Lugares Negocios',
                       'Lugares Primera Clase',
                       'Fecha de Salida',
                       'Estado']

    def aniadir_vuelo(self, destino, h_salida, h_llegada, c_turista, c_negocios,
                      c_primera_clase, cupo_t, cupo_n, cupo_pc, fecha_salida, estado: bool = True):
        """Añade un vuelo

        Parametros
        --------------------
        destino -- El destino
        h_salida -- Hora de Salida
        h_llegada -- Hora de llegada
        c_turista -- Costo de Clase turista
        c_negocios -- Costo de Calse Negocios
        c_primera_clase -- Costo Primera Clase
        cupo_t -- Cupo Clase Turista
        cupo_n -- Costo Clase Negocios
        cupo_pc -- Costo Primera Clase
        fecha_salida -- Costo Fecha Salida
        estado -- Estado Si es 'False' esta cancelado (default True)
        """

        if self.identificador <= self.get_max_vuelos():
            nuevo = {
                'Destino': destino,  # 1
                'Hora de salida': h_salida,  # 2
                'Hora de llegada': h_llegada,  # 3
                'Costo clase Turista': c_turista,  # 4
                'Costo clase Negocios': c_negocios,  # 5
                'Costo Primera clase': c_primera_clase,  # 6
                'Lugares Turista': cupo_t,  # 7
                'Lugares Negocios': cupo_n,  # 8
                'Lugares Primera Clase': cupo_pc,  # 9
                'Fecha de Salida': fecha_salida,  # 10
                'Estado': 'Normal' if estado else 'Cancelado'  # 11
            }
            self.__vuelo[self.identificador] = nuevo
            self.identificador += 1
            return True
        else:
            return False

    def listar_vuelos(self):
        """Devueve los vuelos en una comoda cadena de carácteres

        Return
        --------------------
        vuelos -- Una cadena de de caracteres con los vuelos
        """

        vuelos = ''
        lista = list(self.__vuelo.values())
        for i in range(len(lista)):
            vuelos += '\n\t Vuelo no. ' + str(i + 1) + ':\n\t\t'
            llave, valores = list(lista[i].keys()), list(lista[i].values())
            for x in range(len(llave)):
                vuelos += llave[x] + ': ' + str(valores[x]) + '\n\t\t'
        return vuelos

    def cancelar_vuelo(self, ide):
        """Cambia el Estado del vuelo a cancelado

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        self.__vuelo[ide]['Estado'] = 'Cancelado'

    def actualizar_destino(self, ide, destino):
        """Actualiza el Destino

        Parametros
        --------------------
        ide -- Identificador del vuelo
        destino -- El destino que se sobreescribirá
        """

        self.__vuelo[ide]['Destino'] = destino

    def actualizar_hora_salida(self, ide, h_salida):
        """Actualiza la hora de salida

        Parametros
        --------------------
        ide -- Identificador del vuelo
        h_salida -- La hora de Salida que se sobreescribirá
        """

        self.__vuelo[ide]['Hora de salida'] = h_salida

    def actualizar_hora_llegada(self, ide, h_llegada):
        """Actualiza la hora de llegada

        Parametros
        --------------------
        ide -- Identificador del vuelo
        h_llegada -- La hora de llegada que se sobreescribirá
        """

        self.__vuelo[ide]['Hora de llegada'] = h_llegada

    def actualizar_costo_turista(self, ide, costo_t):
        """Actualiza el costa de la clase turista

        Parametros
        --------------------
        ide -- Identificador del vuelo
        costo_t -- El costo de la clase Turista que se sobreescribirá
        """

        self.__vuelo[ide]['Costo clase Turista'] = costo_t

    def actualizar_costo_negocios(self, ide, costo_n):
        """Actualiza el costa de la clase negocio

        Parametros
        --------------------
        ide -- Identificador del vuelo
        costo_n -- El costo de la clase Negocios que se sobreescribirá
        """

        self.__vuelo[ide]['Costo clase Negocios'] = costo_n

    def actualizar_costo_primera_clase(self, ide, costo_pc):
        """Actualiza el costa de la primera clase

        Parametros
        --------------------
        ide -- Identificador del vuelo
        costo_pc -- El costo de la Primera clase que se sobreescribirá
        """

        self.__vuelo[ide]['Costo Primera clase'] = costo_pc

    def actualizar_lugares_turista(self, ide, cupo_t):
        """Actualiza los lugares de la clase Turista

        Parametros
        --------------------
        ide -- Identificador del vuelo
        cupo_t -- Los lugares de la clase Turista que se sobreescribirá
        """

        self.__vuelo[int(ide) + 1]['Lugares Turista'] = cupo_t

    def actualizar_lugares_negocios(self, ide, cupo_n):
        """Actualiza los lugares de la clase Negocios

        Parametros
        --------------------
        ide -- Identificador del vuelo
        cupo_n -- Los lugares de la clase Negocios que se sobreescribirá
        """

        self.__vuelo[int(ide) + 1]['Lugares Negocios'] = cupo_n

    def actualizar_lugares_primera_clase(self, ide, cupo_pc):
        """Actualiza los lugares de la primera clase

        Parametros
        --------------------
        ide -- Identificador del vuelo
        cupo_pc -- Los lugares de la Primera clase que se sobreescribirá
        """

        self.__vuelo[int(ide) + 1]['Lugares Primera Clase'] = cupo_pc

    def actualizar_fecha_salida(self, ide, f_salida):
        """Actualiza la fecha de salida

        Parametros
        --------------------
        ide -- Identificador del vuelo
        f_salida -- La fecha de salida que se sobreescribirá
        """

        self.__vuelo[int(ide) + 1]['Fecha de Salida'] = f_salida

    def actualizar_estado(self, ide, edo):
        """Cambia el estado del vuelo

        Parametros
        --------------------
        ide -- Identificador del vuelo
        edo -- Estado a cambiar
        """

        self.__vuelo[ide]['Estado'] = edo

    def get_destino(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[ide]['Destino']

    def get_hora_salida(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[ide]['Hora de salida']

    def get_hora_llegada(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[ide]['Hora de llegada']

    def get_costo_clase_turista(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Costo clase Turista']

    def get_costo_clase_negocios(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Costo clase Negocios']

    def get_costo_primera_clase(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Costo Primera clase']

    def get_lugares_turista(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Lugares Turista']

    def get_lugares_negocios(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Lugares Negocios']

    def get_lugares_primera_clase(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[int(ide) + 1]['Lugares Primera Clase']

    def get_fecha_salidas(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[ide]['Fecha de Salida']

    def get_estado(self, ide):
        """Regresa el estado del vuelo en un buleano

        Parametros
        --------------------
        ide -- Identificador del vuelo
        """

        return self.__vuelo[ide]['Estado']

    def sumar_caja(self, dinero):
        """Se incrementa el saldo de la caja

        Parametros
        --------------------
        dinero -- Dinero a ingresar
        """

        self.__caja += dinero

    def sumar_caja_tarjeta(self, dinero):
        """Se incrementa el saldo de la caja

        Parametros
        --------------------
        dinero -- Dinero a ingresar
        """

        self.__caja_tarjeta += dinero

    def mas_pago_tarjeta(self):
        """Incrementa el contador de los pagos hechos con tarjeta"""

        self.__pagos_tarjeta += 1

    def mas_pago_efectivo(self):
        """Incrementa el contador de los pagos hechos con efectivo"""

        self.__pagos_efectivo += 1

    def get_pagos_tarjeta(self):
        """Obtiene la cantidad de pagos hechas con tarjeta"""

        return self.__pagos_tarjeta

    def get_pagos_efectivo(self):
        """Obtiene la cantidad de pagos hechas con efectivo"""

        return self.__pagos_efectivo

    def actualizar_caja(self, money):
        """Actualiza el dinero de la caja"""

        self.__caja += money

    def actualizar_caja_tarjeta(self, money):
        """Actualiza el dinero de la caja"""

        self.__caja_tarjeta += money

    def get_caja(self):
        """Obtiene el dinero de la caja"""

        return self.__caja

    def get_caja_tarjeta(self):
        """Obtiene el dinero obtenido por las tarjetas"""

        return self.__caja_tarjeta

    def mas_vendido_turista(self):
        """Actualiza el contador de la clase turista"""

        self.__vendidos_turista += 1

    def mas_vendido_negocios(self):
        """Actualiza el contador de la clase negocios"""

        self.__vendidos_negocios += 1

    def mas_vendido_primera_clase(self):
        """Actualiza el contador de la primera clase"""

        self.__vendidos_primera_clase += 1

    def get_vendidos_turista(self):
        """Obtiene el numero de boletos vendidos de la clase turista"""

        return self.__vendidos_turista

    def get_vendidos_negocios(self):
        """Obtiene el numero de boletos vendidos de la clase negocios"""

        return self.__vendidos_negocios

    def get_vendidos_primera_clase(self):
        """Obtiene el numero de boletos vendidos de la primera clase"""

        return self.__vendidos_primera_clase

    def set_max_vuelos(self, num):
        """Asigna el número máximo de vuelos"""

        self.__max_vuelos = num

    def get_max_vuelos(self):
        """Obtiene el número maximo de vuelos"""

        return self.__max_vuelos

    def listar_datos(self):
        cad = '\n'
        for x in range(len(self.plantilla_datos)):
            cad += '\t\t' + str(x) + '. ' + self.plantilla_datos[x] + '\n'
        return cad

    def listar_vuelos_disponibles(self):
        """Lista los vuelos disponibles"""

        vuelos = ''
        vuelo_valores = list(self.__vuelo.values())
        for i in range(len(vuelo_valores)):
            print(vuelo_valores[i]['Lugares Turista'])
            if not (vuelo_valores[i]['Lugares Turista'] == 0 and\
               vuelo_valores[i]['Lugares Negocios'] == 0 and\
               vuelo_valores[i]['Lugares Primera Clase'] == 0):
                vuelos += '\tVUELO ' + str(i) + '\n'
                llave, valores = list(vuelo_valores[i].keys()), list(vuelo_valores[i].values())
                for x in range(len(llave)):
                    vuelos += '\t' + llave[x] + ': ' + str(valores[x]) + '\n'
            else:
                vuelos += '\n\tVuelo ' + str(i) + ' LLENO'
        return vuelos

    def comprar_vuelo(self, id_vuelo, id_persona, clase, pago):
        """A traves del ide del vuelo y de la persona,
        se compra un vuelo y los identifico

        Parametros
        --------------------
        id_vuelo -- Identificador del vuelo
        id_persona -- Identificador de la persona
        clase -- Clase objetivo a comprar
        pago -- El Varo a que va a dar el cliente

        """

        if self.compra_clase(id_vuelo, clase, pago):
            if id_vuelo in self.__vuelo_persona:
                self.__vuelo_persona[int(id_vuelo)+1] =\
                    self.__vuelo_persona[int(id_vuelo)+1].append(int(id_persona))
            else:
                self.__vuelo_persona[int(id_vuelo)+1] = [int(id_persona)]
            return True
        else:
            return False

    def vuelos_persona(self, id_persona):
        """Devuelve los vuelos de la persona"""

        vuelos = ' '

        vuelo_list = list(self.__vuelo_persona.values())
        for i in range(len(vuelo_list)):
            if id_persona in vuelo_list.pop():
                vuelos += str(i) + ', '
        return vuelos[:-2]

    def compra_clase(self, id_vuelo, clase, pago):
        """Hace una compra de una clase, validando si esta esta llena o no
        Incrementa el contador de su respectiva clase

        Retorna un True si fue un exito

        Parametros
        --------------------
        id_vuelo -- Identificador del vuelo
        clase -- Clase objetivo
        pago -- El varo a dar
        """

        if clase == '1':
            lugares = self.get_lugares_turista(id_vuelo)
            if lugares != 0:
                self.actualizar_lugares_turista(id_vuelo, lugares - 1)
                self.mas_vendido_turista()
                if pago == '1':
                    self.actualizar_caja(self.get_costo_clase_turista(int(id_vuelo)))
                    self.mas_pago_efectivo()
                else:
                    self.actualizar_caja_tarjeta(self.get_costo_clase_turista(int(id_vuelo)))
                    self.mas_pago_tarjeta()
                return True
            else:
                return False

        elif clase == '2':
            lugares = self.get_lugares_negocios(id_vuelo)
            if lugares != 0:
                self.actualizar_lugares_negocios(id_vuelo, lugares - 1)
                self.mas_vendido_negocios()

                if pago == '1':
                    self.actualizar_caja(self.get_costo_clase_negocios(int(id_vuelo)))
                else:
                    self.actualizar_caja_tarjeta(self.get_costo_clase_negocios(int(id_vuelo)))
                    self.mas_pago_tarjeta()
                return True
            else:
                return False

        elif clase == '3':
            lugares = self.get_lugares_primera_clase(id_vuelo)
            if lugares != 0:
                self.actualizar_lugares_primera_clase(id_vuelo, lugares - 1)
                self.mas_vendido_primera_clase()

                if pago == '1':
                    self.actualizar_caja(self.get_costo_primera_clase(int(id_vuelo)))
                else:
                    self.actualizar_caja_tarjeta(self.get_costo_primera_clase(int(id_vuelo)))
                    self.mas_pago_tarjeta()
                return True
            else:
                return False

    def get_clase_by_id(self, id_clase):
        """Devuele en nombre de la clase a traves de un ide convencional"""

        clase = {'1': 'Turista', '2': 'Negocios', '3': 'Primera'}
        return clase[id_clase]
