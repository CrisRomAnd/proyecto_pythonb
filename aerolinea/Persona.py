

class Persona:
    """Clase que define a una persona
    """

    identificador = 1
    personas = dict()
    
    def aniadir_persona(self, usuario, nombre, apellido, password, tipo=None, vuelo=list()):
        """Añade un nuevo usuario a la base de datos

        Parametros
        --------------------
        usuario -- El nombre de usuario que se va a asignar
        nombre -- El nombre del usuario
        apellido -- El apellido del usuario
        password -- La contraseña del usuario
        tipo -- El tipo de usuario que se va dar de alta, si es admin o no (default=None)
        vuelo -- Una lista vacia de vuelos del usuario (default=list())
        """
        nuevo = {
            'usuario': usuario,
            'nombre': nombre,
            'apellido': apellido,
            'pass': password,
            'tipo': tipo,
            'vuelos': vuelo
        }
        self.personas[self.identificador] = nuevo
        self.identificador += 1

    def get_personas(self):
        return self.personas

    def get_id(self, usuario):
        """Obtiene id mediante usuario

        Parametros
        --------------------
        usuario -- El numbre de usuarui único
        """
        lista = list(self.personas.values())
        for i in range(len(lista)):
            us = lista[i]
            if us['usuario'] == usuario:
                return i
        return None

    def val_session(self, ide, contra):
        """Valida la sesion del con su identificador y contraseña

        Parametros
        --------------------
        ide -- El identificador del usuario
        contra -- La contraseña del usuario
        """

        if ide == None:
            return False
        user = list(self.personas.values())[ide]
        return True if user['pass'] == contra else False

    def ultimo_registrado(self):
        """Retorna el último usuario registrado"""

        ultimo = list(self.personas.values())[-1]
        return ultimo['usuario']

    def val_admin(self, ide):
        """Valida si es Administrador

        Parametros
        --------------------
        ide -- El identificador del usuario
        """

        if ide == None:
            return False
        tipo = list(self.personas.values())[ide]
        if tipo['tipo'] == 1:
            return  True
        else:
            False

    def info_usuario(self, ide):
        """Retorna la informacion completa del

        Parametros
        --------------------
        ide -- El identificador del usuario
        """

        info = ''
        user = list(self.personas.values())[ide]
        user_llave, user_valor= list(user.keys()), list(user.values())
        for i in range(len(user_valor) - 1):
            if not (user_llave[i] == 'pass' or user_llave[i] == 'tipo'):
                info += '\n\t' + user_llave[i] + ': ' + user_valor[i]
        return info
